package loyaltyservice

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class LoyaltyServiceSpec extends Specification {

    LoyaltyService loyaltyService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Loyalty(...).save(flush: true, failOnError: true)
        //new Loyalty(...).save(flush: true, failOnError: true)
        //Loyalty loyalty = new Loyalty(...).save(flush: true, failOnError: true)
        //new Loyalty(...).save(flush: true, failOnError: true)
        //new Loyalty(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //loyalty.id
    }

    void "test get"() {
        setupData()

        expect:
        loyaltyService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Loyalty> loyaltyList = loyaltyService.list(max: 2, offset: 2)

        then:
        loyaltyList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        loyaltyService.count() == 5
    }

    void "test delete"() {
        Long loyaltyId = setupData()

        expect:
        loyaltyService.count() == 5

        when:
        loyaltyService.delete(loyaltyId)
        sessionFactory.currentSession.flush()

        then:
        loyaltyService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Loyalty loyalty = new Loyalty()
        loyaltyService.save(loyalty)

        then:
        loyalty.id != null
    }
}
