package loyaltyservice

class BootStrap {

    def init = { servletContext ->

        new Loyalty(uuid: "abracadabra", points: 30).save()
    }
    def destroy = {
    }
}
