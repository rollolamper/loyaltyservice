package loyaltyservice

import grails.gorm.services.Service

@Service(Loyalty)
interface LoyaltyService {

    Loyalty get(Serializable id)

    Loyalty save(Loyalty loyalty)

}