package loyaltyservice

import grails.converters.JSON
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class LoyaltyController {

    LoyaltyService loyaltyService

    static responseFormats = ['json']

    def show(String uuid) {
        render Loyalty.findByUuid(uuid) as JSON
    }

}
