package loyaltyservice

class UrlMappings {

    static mappings = {
        get "/$controller/$uuid(.$format)"(action:"show")

        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
