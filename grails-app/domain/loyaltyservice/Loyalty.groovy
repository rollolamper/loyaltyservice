package loyaltyservice


import grails.rest.*

@Resource(readOnly = true, formats = ['json'])
class Loyalty {

    String uuid
    Integer points

    static constraints = {
        uuid  blank: false, unique: true, nullable: false
        points blank: false, nullable: false, min: 0
    }

}